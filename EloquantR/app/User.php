<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Resource;
use DB;
use App\Project;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function resources(){
        return $this->belongsToMany(Resource::class);
    }

    // public function projects(){
    //     return $this->hasMany(
        
    
    // }

    // public function userProjects(){
    //     return $this->hasMany(Resource::class, 'resource','project_id')->belongsTo(Resource::class, 'resource_users','user_id');
    // }



    // public function projects(){
    //     return $this->hasManyThrough(
    //         Project::class,
    //         Resource::class
    //         // // User::class,
    //         // 'project_id',
    //         // 'id',
    //         // 'id',
    //         // 'id'
    //     );
    // }

    
}
